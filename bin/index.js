#!/usr/bin/env node
const { Command } = require('commander');
const updateChk = require('../lib/update')
const downloadTpl = require('../lib/download')
const fsextra = require('fs-extra')
const inquirer = require('inquirer')
const path = require('path')
const program = new Command();

// version
program
    .version(require('../package.json').version, '-v, --version')

// upgrade
program
	.command('upgrade')
	.description("Check the VuePress-Creator's version.")
    .option('-t, --templete', 'Upgrade the VuePress templete version.')
    .action(() => {
        let _length = process.argv.slice(2).length
        // console.log(_length)
        if ( _length > 2) {
			program.outputHelp()
		} else {
            switch (_length){
                case 1:
                  if (process.argv[2] == 'upgrade') {
                    updateChk()
                  }
                  break
                case 2:
                  if (process.argv[3] == '-t') {
                    console.log(22)
                  }
                  break
                default:
                  program.outputHelp()
            }
        }
    })

// init
program
    .name('iboot-hello-cli')
    .usage('<commands> [options]')
	.command('init <project>')
	.description('Create a VuePress project.')
    .action(project => {
        fsextra.pathExists(project).then(exists => {
            if (!exists) {
              const templatePath = path.resolve(__dirname, '../template')
              const processPath = process.cwd()
              const targetPath = `${processPath}/${project}`
              fsextra.emptyDir(templatePath).then(() => {
                  downloadTpl(templatePath).then(() => {
                    fsextra.copy(templatePath, targetPath).then(() => {
                      fsextra.remove(templatePath)
                    }).catch(err => {
                      console.log(symbols.error, chalk.red('Copy err.'))
                    })
                  })
                }).catch(err => {
                  console.error(err)
              })
            } else {
                console.log(symbols.error, chalk.red('The project already exists.'))
            }
        })
    })    

// help
program.on('--help', function() {
    console.log('')
    console.log('Examples:')
    console.log('  $ vuepress-creator init project')
    console.log('  $ vuepress-creator upgrade -t')
    console.log('')
})        
program.parse(process.argv);        